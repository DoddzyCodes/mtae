#include <iostream>
#include "Service.h"
#include "ThreadManager.h"
#include "TaskManager.h"

#include <chrono>

class TimedService : public Service
{
public:
	struct AddNumbersArg
	{
		int numA;
		int numB;
	};

	TimedService() : m_lUpdateTime(1000) { }
	~TimedService() {}


	void Task_AddSomeNumbers(std::unique_ptr<TaskManager::TTask>& pTask)
	{
		TaskManager::Task<AddNumbersArg, int>* pAddTask = (TaskManager::Task<AddNumbersArg, int>*)pTask.get();

		pAddTask->result.set_value(AddNumbers(pAddTask->argument.numA, pAddTask->argument.numB));
	}

	void Update() override
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(m_lUpdateTime));
		std::cout << "This is timed service " << m_lUpdateTime << " on thread " << std::this_thread::get_id() << " checking in!" << std::endl;
		
		//Check if we have any tasks to complete?
		while(std::unique_ptr<TaskManager::TTask> pTask = m_pTaskManager->GetNextTask(m_uiID))
		{
			if (pTask->uiTaskID == 0) //Temp ID is 0
			{
				Task_AddSomeNumbers(pTask);
				std::cout << m_lUpdateTime << " ran task AddSomeNumbers" << std::endl;
			}
		}

		//For a test, get a random service to add two numbers
		if (m_uiID != 2)
		{
			std::future<int> result = m_pTaskManager->SetTask<AddNumbersArg, int>(2, 0, AddNumbersArg{ 5, 1 });
			result.wait(); //Wait for the result to come in from the other thread
			std::cout << "Result for task is: " << result.get() << ", thread: " << m_lUpdateTime << std::endl;
		}
	}

	void SetUpdateTime(long long fUpdateTime) { m_lUpdateTime = fUpdateTime; }

	int AddNumbers(int a, int b)
	{
		return a + b;
	}
private:
	long long m_lUpdateTime;
	std::future<int> 
};


int main()
{
	ThreadManager threadMgr;
	TaskManager tskManager;

	threadMgr.SetThreadNumber(3);

	std::shared_ptr<TimedService> s1 = std::make_shared<TimedService>();
	std::shared_ptr<TimedService> s2 = std::make_shared<TimedService>();
	std::shared_ptr<TimedService> s3 = std::make_shared<TimedService>();

	s1->SetTaskManager(&tskManager); s1->SetID(1);
	s2->SetTaskManager(&tskManager); s2->SetID(2);
	s3->SetTaskManager(&tskManager); s3->SetID(3);

	s1->SetUpdateTime(1000);
	s2->SetUpdateTime(2000);
	s3->SetUpdateTime(3000);

	threadMgr.AttachServiceToThread(0, s1);
	threadMgr.AttachServiceToThread(1, s2);
	threadMgr.AttachServiceToThread(2, s3);

	threadMgr.LaunchThreads();

	std::this_thread::sleep_for(std::chrono::seconds(10));

	threadMgr.ShutdownThreads();

	return 0;
}