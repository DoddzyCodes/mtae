#pragma once

#include <thread>
#include <list>
#include <vector>
#include <memory>
#include <mutex>

//Temp
#include "Service.h"

class ThreadManager
{
public:

	ThreadManager();
	~ThreadManager();

	void SetThreadNumber(unsigned int a_uiThreadNumber);
	void AttachServiceToThread(unsigned int a_uiThreadID, const std::shared_ptr<Service>& a_pService);

	void LaunchThreads();
	void ShutdownThreads();

private:
	class ServiceThreadHandler
	{
	public:
		enum ThreadState
		{
			Running,
			Stopping,
			Stopped
		};

	public:
		ServiceThreadHandler();
		~ServiceThreadHandler();
		void Run(); //Called by the thread to start;

		void Step(); //Updates the services attached
		
		void AddService(const std::shared_ptr<Service>& pService); //Add a new service to be handled
		void RemoveService(const std::shared_ptr<Service>& pService); //Remove a service from being handled
		std::list<std::shared_ptr<Service>> GetAttachedServices() { return m_vServices; }

		void Reset(); //Reset this handler to the default state
		void Stop(); //Flag the Run() function to stop running
		ThreadState GetThreadState() { return m_eState; }
	private:
		std::list<std::shared_ptr<Service>> m_vServices;
		bool								  m_bKeepRunning;
		std::mutex							  m_dataMutex; //Is a mutex needed if we are only ever writting to the class from one thread?
		ThreadState							  m_eState;
	};
	struct ThreadData
	{
		ThreadData() : handler(new ServiceThreadHandler()){}
		ThreadData(ThreadData&& cpy) : handler(std::move(handler)), thread(std::move(thread)) {};
		ThreadData& operator=(ThreadData&& cpy)
		{
			handler = std::move(cpy.handler);
			thread = std::move(cpy.thread);
		};

		std::thread								thread;
		std::unique_ptr<ServiceThreadHandler>	handler;
	};


	void LaunchSingleThread(unsigned int a_uiThreadNumber);
	void ShutdownSingleThread(unsigned int a_uiThreadNumber);
	
	bool						m_bThreadsStarted;
	std::vector<ThreadData>		m_vThreadList;
	unsigned int				m_uiNumThreads;
};
