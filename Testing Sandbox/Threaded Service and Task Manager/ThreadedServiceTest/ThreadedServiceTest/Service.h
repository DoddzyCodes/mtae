#pragma once

class ServiceManager;
class TaskManager;

class Service
{
public:
	Service();
	~Service();

	Service(ServiceManager* a_pManager);

	virtual void Initialize();;
	virtual void Shutdown();;
	
	virtual void Update();;

	void SetTaskManager(TaskManager* a_pManager);
	//Temp
	void SetID(unsigned int a_uiID) { m_uiID = a_uiID; }

protected:
	unsigned int m_uiID;
	//Temp
	TaskManager* m_pTaskManager;
};
