#include "ThreadManager.h"

#include <thread>

#include "pempek_assert.h"

ThreadManager::ServiceThreadHandler::ServiceThreadHandler()
{
	m_bKeepRunning = true;
	m_eState = Stopped;
}

ThreadManager::ServiceThreadHandler::~ServiceThreadHandler()
{

}

void ThreadManager::ServiceThreadHandler::Run()
{
	m_eState = Running;
	while (m_bKeepRunning)
	{
		Step();
	}
}

void ThreadManager::ServiceThreadHandler::Step()
{
	//Use trylock here - if we can successfully lock, do so, otherwise just skip the step until we can
	if (m_dataMutex.try_lock())
	{
		for (auto pService : m_vServices)
		{
			pService->Update();
		}
		m_dataMutex.unlock();
	}
}

void ThreadManager::ServiceThreadHandler::AddService(const std::shared_ptr<Service>& pService)
{
	m_dataMutex.lock();
	m_vServices.push_back(pService);
	m_dataMutex.unlock();
}

void ThreadManager::ServiceThreadHandler::RemoveService(const std::shared_ptr<Service>& pService)
{
	m_dataMutex.lock();
	m_vServices.remove(pService);
	m_dataMutex.unlock();
}

void ThreadManager::ServiceThreadHandler::Reset()
{
	//m_vServices.clear();
	m_bKeepRunning = true;
	m_eState = Stopped;
}

void ThreadManager::ServiceThreadHandler::Stop()
{
	m_eState = Stopping;
	m_bKeepRunning = false;
}

ThreadManager::ThreadManager()
: m_uiNumThreads(0)
{

}

ThreadManager::~ThreadManager()
{
	ShutdownThreads();
}

void ThreadManager::LaunchThreads()
{
	if (!m_bThreadsStarted)
	{
		for (unsigned int i = 0; i < m_uiNumThreads; ++i)
		{
			LaunchSingleThread(i);
		}
		m_bThreadsStarted = true;
	}
}

void ThreadManager::ShutdownThreads()
{
	if (m_bThreadsStarted)
	{
		for (unsigned int i = 0; i < m_uiNumThreads; ++i)
		{
			ShutdownSingleThread(i);
		}
		m_bThreadsStarted = false;
	}
}

void ThreadManager::LaunchSingleThread(unsigned int a_uiThreadNumber)
{
	ThreadData& newThread = m_vThreadList[a_uiThreadNumber];
	newThread.handler->Reset(); //Reset the handler to the default state
	newThread.thread = std::thread([&]() { newThread.handler->Run(); });

	//m_vThreadList[a_uiThreadNumber] = newThread;
}

void ThreadManager::ShutdownSingleThread(unsigned int a_uiThreadNumber)
{
	ThreadData& thread = m_vThreadList[a_uiThreadNumber];
	thread.handler->Stop();
	thread.thread.join();

}

void ThreadManager::SetThreadNumber(unsigned int a_uiThreadNumber)
{
	//Not sure how to deal with this yet?  Maybe we
	//can have protected threads, and non-protected threads?
	if (m_bThreadsStarted && a_uiThreadNumber < m_uiNumThreads)
	{
		unsigned int uiThreadDifference = m_uiNumThreads - a_uiThreadNumber;
		for (unsigned int i = 0; i < uiThreadDifference; ++uiThreadDifference)
		{
			ShutdownSingleThread(i);
		}
	}
	else if (m_bThreadsStarted && a_uiThreadNumber > m_uiNumThreads)
	{
		unsigned int uiThreadDifference = a_uiThreadNumber - m_uiNumThreads;
		for (unsigned int i = 0; i < uiThreadDifference; ++uiThreadDifference)
		{
			LaunchSingleThread(i);
		}
	}
	else
	{
		m_uiNumThreads = a_uiThreadNumber;
		m_vThreadList.resize(m_uiNumThreads);
	}
}

void ThreadManager::AttachServiceToThread(unsigned int a_uiThreadID, const std::shared_ptr<Service>& a_pService)
{
	ThreadData& thread = m_vThreadList[a_uiThreadID];
	thread.handler->AddService(a_pService);
}