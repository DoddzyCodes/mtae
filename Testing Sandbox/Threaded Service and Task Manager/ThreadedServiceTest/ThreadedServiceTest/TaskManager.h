#pragma once

#include <map>
#include <functional>
#include <future>
#include <list>

class TaskManager
{
public:
	struct TTask
	{
		unsigned int uiTargetService;
		unsigned int uiTaskID;
	};

	template<typename argType, typename retType>
	struct Task : public TTask
	{
		argType argument;
		std::promise<retType> result;
	};

	TaskManager() {};
	~TaskManager() {};

	template<typename argType, typename retType>
	std::future<retType> SetTask(unsigned int uiServiceID, unsigned int uiTaskID, argType argData)
	{
		Task<argType, retType>* pNewTask = new Task<argType, retType>();
		pNewTask->uiTargetService = uiServiceID;
		pNewTask->uiTaskID = uiTaskID;
		pNewTask->argument = argData;

	
		std::lock_guard<std::mutex> lk(m_taskMapMutex);
		m_taskMap[uiServiceID].push_back(std::unique_ptr<TTask>(pNewTask));

		return std::move(pNewTask->result.get_future());
	}

	std::unique_ptr<TTask> GetNextTask(unsigned int uiServiceID)
	{
		std::lock_guard<std::mutex> lk(m_taskMapMutex);

		std::list<std::unique_ptr<TTask>>& taskList = m_taskMap[uiServiceID];

		if (!taskList.empty())
		{
			std::unique_ptr<TTask> tsk = std::move(taskList.front());
			taskList.pop_front();

			return tsk;
		}
		else
		{
			return nullptr;
		}
	}
private:
	std::mutex												  m_taskMapMutex;
	std::map<unsigned int, std::list<std::unique_ptr<TTask>>> m_taskMap;
};